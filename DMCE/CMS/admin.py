# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from .models import *
from easy_thumbnails.files import get_thumbnailer
from image_cropping import ImageCroppingMixin
from solo.admin import SingletonModelAdmin
from jet.admin import CompactInline


class ConfigGalleryInline(ImageCroppingMixin, CompactInline):
    model = ConfigImage
    extra = 0


class ConfigAdmin(ImageCroppingMixin, SingletonModelAdmin):
    inlines = [ConfigGalleryInline, ]
    fieldsets = (
            (u"Datos Generales", {
                'fields': ('logo', 'cropping', 'name_ecomerce', ('phone', 'email'), 'address','location', 'cif', 'description', )
            }),
            (u"SEO", {
                'fields': ('meta_title', 'meta_description', 'meta_keywords')
            }),
            (u"Aviso Legal", {
                'fields': ('legal',)
            }),
        )


class AboutImageInline(ImageCroppingMixin, CompactInline):
    model = AboutImage
    extra = 0


class TeamInline(ImageCroppingMixin, CompactInline):
    model = Member
    extra = 0


class AboutAdmin(SingletonModelAdmin):
    inlines = [AboutImageInline, TeamInline,]
    fieldsets = (
            (u"Datos Sobre Nosotros", {
                'fields': ('text',)
            }),
            (u"SEO", {
                'fields': ('meta_title', 'meta_description', 'meta_keywords')
            }),
        )


class PortfolioImageInline(ImageCroppingMixin, CompactInline):
    model = PortfolioImage
    extra = 0


class PortfolioAdmin(admin.ModelAdmin):
    fieldsets = (
            (u"Información general", {
                'fields': ('title', 'description',)
            }),
            (u"SEO", {
                'fields': ('meta_title', 'meta_description', 'meta_keywords')
            }),
        )
    inlines = [PortfolioImageInline,]


class ServiceImageInline(ImageCroppingMixin, CompactInline):
    model = ServiceImage
    extra = 0


class ServiceAdmin(admin.ModelAdmin):
    list_display = ('title', 'stype', 'featured')
    fieldsets = (
            (u"Información general", {
                'fields': ('title', 'stype', 'description', 'featured', 'short_description')
            }),
            (u"SEO", {
                'fields': ('meta_title', 'meta_description', 'meta_keywords')
            }),
        )
    inlines = [ServiceImageInline,]


class ServiceTypeAdmin(admin.ModelAdmin):
    fieldsets = (
            (u"Información general", {
                'fields': ('title', 'description', )
            }),
            (u"SEO", {
                'fields': ('meta_title', 'meta_description', 'meta_keywords')
            }),
        )

class PartnerAdmin(ImageCroppingMixin, admin.ModelAdmin):
    pass


class PostImageInline(ImageCroppingMixin, CompactInline):
    model = PostImage
    extra = 0


class PostAdmin(admin.ModelAdmin):
    readonly_fields = ['posted',]
    list_display = ('title', 'category', 'posted')
    fieldsets = (
            (u"Información general", {
                'fields': ('title', 'body', 'category', 'posted')
            }),
            (u"SEO", {
                'fields': ('meta_title', 'meta_description', 'meta_keywords')
            }),
        )
    inlines = [PostImageInline,]



admin.site.register(Config, ConfigAdmin)
admin.site.register(About, AboutAdmin)
#admin.site.register(Portfolio, PortfolioAdmin)
#admin.site.register(Service, ServiceAdmin)
#admin.site.register(ServiceType, ServiceTypeAdmin)
#admin.site.register(Partner, PartnerAdmin)
admin.site.register(Contact)
admin.site.register(SocialLink)
#admin.site.register(Category)
#admin.site.register(Post, PostAdmin)
#admin.site.register(Testimony)
