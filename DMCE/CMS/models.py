# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from image_cropping import ImageRatioField
from django.template.defaultfilters import slugify
from easy_thumbnails.files import get_thumbnailer
from solo.models import SingletonModel
from datetime import date
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from location_field.models.plain import PlainLocationField

###CMS###

class AbstractImage(models.Model):
    image = models.ImageField(upload_to='gallery', max_length=500, verbose_name='Imagen (Recomendado 1200x480)')
    text = models.TextField(verbose_name='Texto', blank=True, null=True)
    
    class Meta:
        abstract = True
        verbose_name = 'Imagen'
        verbose_name_plural = u'Galería'

    def __unicode__(self):
        return u'Imagen: ' + unicode(self.id)


class Config(models.Model):
    name_ecomerce = models.CharField(max_length=30, default="", blank=True, verbose_name='Nombre de comercio')
    logo = models.ImageField(verbose_name=u"Logo de la Web", null=True, blank=True)
    cropping = ImageRatioField('logo', '400x100', verbose_name=u'Recorte visible')
    phone = models.CharField(max_length=23, default="", blank=True, verbose_name='Teléfono de contacto')
    email = models.CharField(max_length=127, default="", blank=True, verbose_name='Email de contacto')
    cif = models.CharField(max_length=50, verbose_name=u'CIF', default="", blank=True)
    address = models.CharField(verbose_name=u'Dirección', blank=True, max_length=200)
    location = PlainLocationField(based_fields=['address'], zoom=7, verbose_name=u'Geocalización', blank=True, null=True)
    description = models.TextField(blank=True, null=True, verbose_name=u'Descripción')
    
    meta_title = models.CharField(max_length=140, verbose_name='Meta-Título', blank=True)
    meta_keywords = models.TextField(verbose_name='Meta-KeyWords', blank=True)
    meta_description = models.TextField(verbose_name='Meta-Description', blank=True)

    legal = RichTextUploadingField(verbose_name=u'Aviso Legal', blank=True, null=True)

    def __unicode__(self):
        return u"Configuración de la Web"

    def get_logo(self):
        return get_thumbnailer(self.logo).get_thumbnail({'size': (366, 63), 'box': self.cropping, 'crop': True, 'detail': True }).url 

    class Meta:
        verbose_name = u"Configuración de la Web"
        verbose_name_plural = u"Configuración de la Web"


class ConfigImage(AbstractImage):
    cropping = ImageRatioField('image', '1200x480', verbose_name=u'Recorte visible')
    config = models.ForeignKey(Config, related_name='gallery', null=True)
    
    def get_img(self):
        return get_thumbnailer(self.image).get_thumbnail({'size': (1200, 480), 'box': self.cropping, 'crop': True, 'detail': True }).url 


class Slug(models.Model):
    slug = models.SlugField(verbose_name=u'URL amigable', unique=True, blank=True, max_length=255)

    def __unicode__(self):
        return self.slug


class SEOModel(models.Model):
    meta_title = models.CharField( max_length=140, verbose_name='Meta-Título', blank=True)
    meta_keywords = models.TextField(verbose_name='Meta-KeyWords', blank=True)
    meta_description = models.TextField(verbose_name='Meta-Description', blank=True)
    slug = models.OneToOneField(Slug, verbose_name=u'URL amigable', blank=True, null=True)

    def save(self, *args, **kwargs):
        if not self.id and self.slug_id == None:
            try:
                id = self.__class__.objects.latest("id").id + 1
            except:
                id = 1
            slug = Slug(slug=slugify(self.title))
            qs = Slug.objects.filter(slug=slug)
            if qs.exists():
                slug = Slug(slug=slugify(self.title)+"-%s" % str(id))
            else:
                slug = Slug(slug=slugify(self.title))
            slug.save()
                
            self.slug =  slug
        super(SEOModel, self).save( *args, **kwargs)
        
    class Meta:
        abstract = True


class Titled(models.Model):
    title = models.CharField(max_length=127, verbose_name=u'Nombre o Título')

    def __unicode__(self):
        return self.title

    class Meta:
        abstract = True


class About(SingletonModel):
    text = models.TextField(verbose_name=u'Texto sobre Nosotros', blank=True, null=True)
    meta_title = models.CharField( max_length=140, verbose_name='Meta-Título', blank=True)
    meta_keywords = models.TextField(verbose_name='Meta-KeyWords', blank=True)
    meta_description = models.TextField(verbose_name='Meta-Description', blank=True)
    
    class Meta:
        verbose_name = u"Sobre Nosotros"


class AboutImage(AbstractImage):
    cropping = ImageRatioField('image', '1200x480', verbose_name=u'Recorte visible')
    about = models.ForeignKey(About, related_name='gallery', null=True)
    
    def get_img(self):
        return get_thumbnailer(self.image).get_thumbnail({'size': (1200, 480), 'box': self.cropping, 'crop': True, 'detail': True }).url 


class ServiceType(SEOModel, Titled):
    description = models.TextField(verbose_name=u'Descripción', blank=True, null=True)

    class Meta:
        verbose_name = u'Tipo de Servicio'
        verbose_name_plural = u'Tipos de Servicios'


class Service(SEOModel, Titled):
    description = RichTextUploadingField(verbose_name=u'Descripción', blank=True, null=True)
    stype = models.ForeignKey(ServiceType, related_name='services', blank=True, null=True, verbose_name='Tipo de Servicio')
    featured = models.BooleanField(verbose_name='Destacado en Portada', default=False)
    short_description = models.TextField(verbose_name=u'Descripción Listado', blank=True, null=True, max_length=100)
    
    class Meta:

        verbose_name = u'Servicio'
        verbose_name_plural = u'Servicios'


class ServiceImage(AbstractImage):
    cropping = ImageRatioField('image', '1200x480', verbose_name=u'Recorte visible')
    service = models.ForeignKey(Service, related_name='gallery', null=True)

    def get_img(self):
        return get_thumbnailer(self.image).get_thumbnail({'size': (1200, 480), 'box': self.cropping, 'crop': True, 'detail': True }).url 


class Portfolio(SEOModel, Titled):
    description = RichTextUploadingField(verbose_name=u'Descripción', blank=True, null=True)

    class Meta:
        verbose_name = u'Portfolio'
        verbose_name_plural = u'Portfolio'


class PortfolioImage(AbstractImage):
    cropping = ImageRatioField('image', '1200x480', verbose_name=u'Recorte visible')
    portfolio = models.ForeignKey(Portfolio, related_name='gallery', null=True)

    def get_img(self):
        return get_thumbnailer(self.image).get_thumbnail({'size': (1200, 480), 'box': self.cropping, 'crop': True, 'detail': True }).url 


class Partner(Titled):
    image = models.ImageField(upload_to='partners', max_length=500, verbose_name='Image (Recomendado 110x80)')
    cropping = ImageRatioField('image', '110x80', verbose_name=u'Recorte visible')

    def get_img(self):
        return get_thumbnailer(self.image).get_thumbnail({'size': (110, 80), 'box': self.cropping, 'crop': True, 'detail': True }).url 

    class Meta:
        verbose_name = 'Partner'
        verbose_name_plural = u'Partners'


class Contact(models.Model):
    name = models.CharField(max_length=127, verbose_name='Nombre', null=True) 
    email = models.EmailField(verbose_name='Email', null=True)
    phone = models.CharField(max_length=30, verbose_name=u'Teléfono', default="", blank=True)
    notes = models.TextField(verbose_name='Mensaje', null=True)

    class Meta:
        verbose_name = "Contacto Web"
        verbose_name_plural = "Contactos Web"


SOCIAL_TYPE = (
    ('fb','Facebook'),
    ('tw','Twitter'),
    ('g+','Google+'),
    ('yt','YouTube'),
    )


class SocialLink(models.Model):
    url = models.URLField(max_length=300, verbose_name=u'URL Red Social')
    stype = models.CharField(max_length=2, verbose_name='Nombre de Red Social', choices=SOCIAL_TYPE)

    def get_faclass(self):
        if self.stype == 'fb':
            return 'fa-facebook'
        elif self.stype == 'tw':
            return 'fa-twitter'
        elif self.stype == 'g+':
            return 'fa-google-plus'
        elif self.stype == 'yt':
            return 'fa-youtube'

    def __str__(self):
        return self.stype

    class Meta:
        verbose_name = u"Link a Red Social"
        verbose_name_plural = u"RRSS"

#About Team
class Person(models.Model):
    name = models.CharField(max_length=127, verbose_name='Nombre', null=True)
    image = models.ImageField(upload_to='images', max_length=500, verbose_name='Imagen (Recomendado 110x80)')
    text = models.TextField(verbose_name='Texto', blank=True, null=True)
    
    class Meta:
        abstract = True

class Member(Person):
    cropping = ImageRatioField('image', '130x98', verbose_name=u'Recorte visible')
    about = models.ForeignKey(About, related_name='team', null=True)

    def get_img(self):
        return get_thumbnailer(self.image).get_thumbnail({'size': (1200, 480), 'box': self.cropping, 'crop': True, 'detail': True }).url 

    def __str__(self):
        return self.name

    class Meta:
        verbose_name='Miembro'
        verbose_name_plural='Equipo'


class Testimony(Person):
    cropping = ImageRatioField('image', '1200x480', verbose_name=u'Recorte visible')

    def get_img(self):
        return get_thumbnailer(self.image).get_thumbnail({'size': (1200, 480), 'box': self.cropping, 'crop': True, 'detail': True }).url 

    class Meta:
        verbose_name='Testimonio'
        verbose_name_plural='Testimonios'

###BLOG###

class Category(SEOModel, Titled):

    class Meta:
        verbose_name = u'Categoría'
        verbose_name_plural = 'Categorías'


class Post(SEOModel, Titled):
    body = RichTextUploadingField(verbose_name='Cuerpo')
    posted = models.DateField(db_index=True, auto_now_add=True, verbose_name=u'Fecha de Publicación')
    category = models.ForeignKey(Category, related_name="posts", verbose_name=u'Categoría')
    
    class Meta:
        verbose_name=u'Publicación'
        verbose_name_plural=u'Publicaciones'

    
class PostImage(AbstractImage):
    cropping = ImageRatioField('image', '1200x480', verbose_name=u'Recorte visible')
    post = models.ForeignKey(Post, related_name='gallery', null=True)

    def get_img(self):
        return get_thumbnailer(self.image).get_thumbnail({'size': (1200, 480), 'box': self.cropping, 'crop': True, 'detail': True }).url 

