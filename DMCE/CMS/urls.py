from django.conf.urls import url
from CMS import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^contacto/$', views.contact, name="contact"),
    url(r'^aviso-legal/$', views.legal, name="legal"),
    url(r'^sobre-nosotros/$', views.about, name="about"),
    url(r'^(?P<slug>[\w-]+)/$', views.dispatch_slug, name="dispatch_slug"),
]