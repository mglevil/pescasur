# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-05-30 00:49
from __future__ import unicode_literals

import ckeditor_uploader.fields
from django.db import migrations, models
import django.db.models.deletion
import image_cropping.fields


class Migration(migrations.Migration):

    dependencies = [
        ('CMS', '0003_auto_20180528_1833'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('meta_title', models.CharField(blank=True, max_length=140, verbose_name='Meta-T\xedtulo')),
                ('meta_keywords', models.TextField(blank=True, verbose_name='Meta-KeyWords')),
                ('meta_description', models.TextField(blank=True, verbose_name='Meta-Description')),
                ('title', models.CharField(max_length=127, verbose_name='Nombre o T\xedtulo')),
                ('slug', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='CMS.Slug', verbose_name='URL amigable')),
            ],
            options={
                'verbose_name': 'Categor\xeda',
                'verbose_name_plural': 'Categor\xedas',
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('meta_title', models.CharField(blank=True, max_length=140, verbose_name='Meta-T\xedtulo')),
                ('meta_keywords', models.TextField(blank=True, verbose_name='Meta-KeyWords')),
                ('meta_description', models.TextField(blank=True, verbose_name='Meta-Description')),
                ('title', models.CharField(max_length=127, verbose_name='Nombre o T\xedtulo')),
                ('body', ckeditor_uploader.fields.RichTextUploadingField(verbose_name='Cuerpo')),
                ('posted', models.DateField(auto_now_add=True, db_index=True, verbose_name='Fecha de Publicaci\xf3n')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='posts', to='CMS.Category', verbose_name='Categor\xeda')),
                ('slug', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='CMS.Slug', verbose_name='URL amigable')),
            ],
            options={
                'verbose_name': 'Publicaci\xf3n',
                'verbose_name_plural': 'Publicaciones',
            },
        ),
        migrations.CreateModel(
            name='PostImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(max_length=500, upload_to='gallery', verbose_name='Image (Recomendado 1200x480)')),
                ('text', models.TextField(blank=True, null=True, verbose_name='Texto')),
                ('cropping', image_cropping.fields.ImageRatioField('image', '1200x480', adapt_rotation=False, allow_fullsize=False, free_crop=False, help_text=None, hide_image_field=False, size_warning=False, verbose_name='Recorte visible')),
                ('post', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='gallery', to='CMS.Post')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'Imagen',
                'verbose_name_plural': 'Galer\xeda',
            },
        ),
        migrations.AlterField(
            model_name='configimage',
            name='text',
            field=models.TextField(blank=True, null=True, verbose_name='Texto'),
        ),
    ]
