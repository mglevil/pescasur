# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-05-28 10:47
from __future__ import unicode_literals

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CMS', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='config',
            name='legal',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Aviso Legal'),
        ),
    ]
