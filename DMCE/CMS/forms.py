# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms
from CMS.models import Contact

class ContactForm(forms.ModelForm):
    name = forms.CharField(label="Nombre", required=True)
    email = forms.EmailField(label=u'Correo electrónico', required=True)
    phone = forms.CharField(label=u'Teléfono')
    
    class Meta:
        model = Contact
        fields = ('name', 'email', 'phone', 'notes')