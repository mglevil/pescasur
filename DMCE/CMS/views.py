# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator
from django.core.mail import EmailMessage
from CMS.models import *
from CMS.forms import *
from ecommerce.models import Category, Banner, Product
from django.conf import settings


def index(request):
    categories = Category.objects.all()
    banners = Banner.objects.all()
    redes_sociales = SocialLink.objects.all()

    #Products in cart
    if 'cart' in request.session and len(request.session['cart'].keys()) > 0:
        cart_items = Product.objects.filter(id__in = request.session['cart'].keys())
        cart_items = [(product, request.session['cart'][str(product.id)]) for product in cart_items]
        cart_tam = len(cart_items)
    
    return render(request, "CMS/index.html", locals())


def legal(request):
    categories = Category.objects.all()
    #Products in cart
    if 'cart' in request.session and len(request.session['cart'].keys()) > 0:
        cart_items = Product.objects.filter(id__in = request.session['cart'].keys())
        cart_items = [(product, request.session['cart'][str(product.id)]) for product in cart_items]
        cart_tam = len(cart_items)

    return render(request, "CMS/generic_content.html", locals())


def about(request):
    categories = Category.objects.all()
    about = About.objects.get_or_create(id=1)[0]
    members = Member.objects.all()
    #Products in cart
    if 'cart' in request.session and len(request.session['cart'].keys()) > 0:
        cart_items = Product.objects.filter(id__in = request.session['cart'].keys())
        cart_items = [(product, request.session['cart'][str(product.id)]) for product in cart_items]
        cart_tam = len(cart_items)

    return render(request, "CMS/about.html", locals())


def contact(request):
    c_form = ContactForm()
    if request.method == 'POST' and 'name' in request.POST:
        c_form = ContactForm(request.POST)
        if c_form.is_valid():
            contact = c_form.save(commit=False)
            contact.save()
            subject = '[NOMBRE NEGOCIO] - Nuevo Formulario de Contacto Web'
            message = 'Datos del Formulario:\n'
            message += u'Nombre: %s\n' % contact.name
            message += u'Email: %s\n' % contact.email
            message += u'Teléfono: %s\n' % contact.phone
            message += u'Mensaje: %s\n' % contact.notes
            send_to = [Config.objects.get_or_create(id=1).email,]
            email = EmailMessage(
                subject,
                message,
                settings.DEFAULT_FROM_EMAIL, 
                send_to,
                )
            email.send()
    return render(request, "CMS/contact.html", locals())


def dispatch_slug(request, slug):
    slug = get_object_or_404(Slug, slug=slug)
    qs = Service.objects.filter(slug=slug)
    if qs.exists():
        content = qs[0]
    qs = Post.objects.filter(slug=slug)
    if qs.exists():
        content = qs[0]
    return render(request, "CMS/generic_content.html", locals())