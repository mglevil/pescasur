#Products in cart
def cart_tam(request):
    if 'cart' in request.session and len(request.session['cart'].keys()) > 0:
        cart_items = Product.objects.filter(id__in = request.session['cart'].keys())
        cart_items = [(product, request.session['cart'][str(product.id)]) for product in cart_items]
    return len(cart_items)