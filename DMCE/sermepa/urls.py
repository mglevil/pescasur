from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns
from sermepa import views

urlpatterns = i18n_patterns = [
    url(r'^$', views.sermepa_ipn, name="sermepa_ipn")
]
