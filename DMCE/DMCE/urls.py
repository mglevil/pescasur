from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static, serve
from django.contrib.auth import views as auth_views
from ecommerce.views import account, activate_view, auth_view, logout_view, sing_up_view, login_view

urlpatterns = [
    url(r'^sermepa/', include('sermepa.urls')),
    url(r'^jet/', include('jet.urls', 'jet')), 
    url(r'^admin/', admin.site.urls),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^static/(?P<path>.*)$', serve, { 'document_root': settings.STATIC_ROOT }), 
    url(r'^media/(?P<path>.*)$', serve, { 'document_root': settings.MEDIA_ROOT, }), 
    url(r'^password_reset/$', auth_views.password_reset, name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', activate_view, name='activate'),
    url(r'^auth/', auth_view, name="auth"),
    url(r'^login/', login_view, name="login"),
    url(r'^logout/', logout_view, name="logout"),
    url(r'^sing_up/', sing_up_view, name="sing_up"),
    url(r'^cuenta/', account, name="account"),
    url(r'^tienda/', include('ecommerce.urls', namespace='ecommerce')),
    url(r'', include('CMS.urls')),
]
