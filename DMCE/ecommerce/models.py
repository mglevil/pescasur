# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from image_cropping import ImageRatioField
from django.template.defaultfilters import slugify
from easy_thumbnails.files import get_thumbnailer
from datetime import date
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from CMS.models import Titled, AbstractImage
from django.db.models.signals import pre_delete, post_save, pre_save, post_delete
from django.dispatch import receiver
from django.utils.crypto import get_random_string
from django.contrib.auth.models import User

#User._meta.get_field('email')._unique = True

class Slug(models.Model):
    slug = models.SlugField(verbose_name=u'URL amigable', unique=True, blank=True, max_length=255)

    def __str__(self):
        return self.slug


class SEOModel(models.Model):
    meta_title = models.CharField( max_length=140, verbose_name='Meta-Título', blank=True)
    meta_keywords = models.TextField(verbose_name='Meta-KeyWords', blank=True)
    meta_description = models.TextField(verbose_name='Meta-Description', blank=True)
    slug = models.OneToOneField(Slug, verbose_name=u'URL amigable', blank=True, null=True)

    def save(self, *args, **kwargs):
        if not self.id and self.slug_id == None:
            try:
                id = self.__class__.objects.latest("id").id + 1
            except:
                id = 1
            slug = Slug(slug=slugify(self.title))
            qs = Slug.objects.filter(slug=slug)
            if qs.exists():
                slug = Slug(slug=slugify(self.title)+"-%s" % str(id))
            else:
                slug = Slug(slug=slugify(self.title))
            slug.save()
                
            self.slug = slug
        super(SEOModel, self).save( *args, **kwargs)
        
    class Meta:
        abstract = True

class Client(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    email_confirmed = models.BooleanField(default=False)
    firstname = models.CharField(max_length=100, verbose_name=u'Nombre', null=True, blank=True)
    lastname = models.CharField(max_length=100, verbose_name=u'Apellidos', null=True, blank=True)
    phone = models.CharField(max_length=30, verbose_name=u'Teléfono', null=True, blank=True)
    country = models.CharField(max_length=100, verbose_name=u'País', null=True, blank=True)
    city = models.CharField(max_length=100, verbose_name=u'Población', null=True, blank=True)
    postal_code = models.CharField(max_length=30, verbose_name=u'Código Postal', null=True, blank=True)
    address = models.CharField(max_length=250, verbose_name=u'Dirección', null=True, blank=True)
    card_id = models.CharField(verbose_name=u'DNI/NIF/CIF', max_length=50, null=True)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name='Cliente'
        verbose_name_plural='Clientes'


@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created and not instance.is_superuser and not instance.is_staff:
        Client.objects.create(user=instance)
        instance.profile.save()


class Category(SEOModel, Titled):
    image = models.ImageField(upload_to='partners', max_length=500, verbose_name='Imagen (Recomendado 1140x642)')
    cropping = ImageRatioField('image', '1140x642', verbose_name=u'Recorte visible')
    description = models.TextField(verbose_name=u'Descripción', blank=True)

    def get_img(self):
        return get_thumbnailer(self.image).get_thumbnail({'size': (110, 80), 'box': self.cropping, 'crop': True, 'detail': True }).url 

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = u'Categoría'
        verbose_name_plural = 'Categorías'
    

class Product(SEOModel, Titled):
    price = models.FloatField(verbose_name='Precio', default=0.00)
    old_price = models.FloatField(verbose_name='Precio Anterior', null=True, blank=True)
    description = models.TextField(verbose_name=u'Descripción', blank=True, null=True)
    specification = RichTextUploadingField(verbose_name=u'Caracteristicas', blank=True, null=True)
    featured = models.BooleanField(default=False, verbose_name=u'Destacado')
    quantity = models.PositiveIntegerField(verbose_name=u'Cantidad', default=0)
    category = models.ForeignKey(Category, related_name='categories', blank=True, null=True, verbose_name=u'Categoría del producto')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name='Producto'
        verbose_name_plural='Productos'
        ordering =['-id', ]


class ProductImage(AbstractImage):
    cropping = ImageRatioField('image', '300x300', verbose_name=u'Recorte visible')
    product = models.ForeignKey(Product, related_name='gallery', null=True)

    def get_img(self):
        return get_thumbnailer(self.image).get_thumbnail({'size': (300, 300), 'box': self.cropping, 'crop': True, 'detail': True }).url 


class Sale(models.Model):
    date = models.DateField(default=date.today,verbose_name='Fecha', editable=True)
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Producto', related_name='sales')
    price = models.FloatField(verbose_name='Precio')

    def __unicode__(self):
        return unicode(self.product.name)

    class Meta:
        verbose_name = 'Venta'
        verbose_name_plural = "Ventas"


STATUS_CHOICES = (
      ("R","Realizado"),  
      ("C","Confirmado"),
      ("P", "Pagado"),
      ("E", "Enviado"),
      ("P", "Preparado"),
    )


def random_token():
    return get_random_string(length=16)


HOUR_CHOICES = (
      ('', '¿Cuando realizará la recogida?'),
      ("MP", "Mañana - Primera hora"),  
      ("MM", "Mañana - Media mañana"),
      ("MU", "Mañana - Última hora"),
      ("TP", "Tarde - Primera hora"),
      ("TM", "Tarde - Media tarde"),
      ("TU", "Tarde - Última hora"),
    )
DAY_CHOICES = (
      ('', '¿Que dia vendra?'),
      ("L", "Lunes"),  
      ("M", "Martes"),
      ("X", "Miércoles"),
      ("J", "Jueves"),
      ("V", "Viernes"),
      ("S", "Sábado"),
    )

class Order(models.Model):
    date = models.DateField(default=date.today, verbose_name='Fecha', editable=False)
    hour_collection = models.CharField(verbose_name='Hora de recogida', choices = HOUR_CHOICES, max_length=2, blank=True)
    day_collection = models.CharField(verbose_name='Día de recogida', choices = DAY_CHOICES, max_length=1, blank=True)
    total = models.FloatField(verbose_name='Total')
    products = models.TextField(verbose_name='Compra')
    client = models.ForeignKey(Client, blank=True, null=True, verbose_name='Cliente', related_name='orders')
    notes = models.TextField(verbose_name=u'Notas sobre el pedido', blank=True, null=True)
    status = models.CharField(u"Estado del pedido", choices = STATUS_CHOICES, max_length=1, blank=True, null=True)
    ref_token = models.CharField(max_length=16, verbose_name='Número de Referencia', editable=False, default=random_token)

    #Facturación
    inv_firstname = models.CharField(max_length=100, verbose_name=u'Nombre')
    #inv_lastname = models.CharField(max_length=100, verbose_name=u'Apellidos')
    inv_phone = models.CharField(max_length=30, verbose_name=u'Teléfono')
    #inv_country = models.CharField(max_length=100, verbose_name=u'País')
    #inv_city = models.CharField(max_length=100, verbose_name=u'Población')
    #inv_postal_code = models.CharField(max_length=30, verbose_name=u'Código Postal')
    #inv_address = models.CharField(max_length=250, verbose_name=u'Dirección')
    inv_email = models.EmailField(verbose_name=u'Correo electrónico')
    inv_card_id = models.CharField(verbose_name=u'DNI/NIF/CIF', max_length=50, null=True)

    #Envío
    ship_firstname = models.CharField(max_length=100, verbose_name=u'Nombre')
    #ship_lastname = models.CharField(max_length=100, verbose_name=u'Apellidos')
    ship_phone = models.CharField(max_length=30, verbose_name=u'Teléfono')
    #ship_country = models.CharField(max_length=100, verbose_name=u'País')
    #ship_city = models.CharField(max_length=100, verbose_name=u'Población')
    #ship_postal_code = models.CharField(max_length=30, verbose_name=u'Código Postal')
    #ship_address = models.CharField(max_length=250, verbose_name=u'Dirección')
    ship_email = models.EmailField(verbose_name=u'Correo electrónico')

    class Meta:
        verbose_name = "Pedido"
        verbose_name_plural = "Pedidos"
        ordering =['-id', ]


    def __unicode__(self):
        return self.inv_firstname+ " - " + str(self.total)


class Invoice(models.Model):
    order = models.OneToOneField('Order', on_delete=models.CASCADE, primary_key=True, related_name='invoice')
    date = models.DateTimeField(u"Fecha", auto_now_add=True)
    total = models.FloatField(verbose_name=u'Total',  null=True)
    invoice_no = models.IntegerField(verbose_name='Número de factura', null=True, default=0)
    name = models.CharField(verbose_name=u'Nombre/Razón Social', max_length=200, null=True)
    surnames = models.CharField(verbose_name=u'Apellidos', max_length=100, blank=True, null=True)
    card_id = models.CharField(verbose_name=u'DNI/NIF/CIF', max_length=50, null=True)
    address = models.CharField(verbose_name=u'Dirección', max_length=200, null=True)
    zip_code = models.CharField(verbose_name=u'Código postal', max_length=50, null=True)
    city = models.CharField(verbose_name=u'Ciudad', max_length=50, null=True)
    country = models.CharField(verbose_name=u'País', max_length=50, null=True)

    class Meta:
        verbose_name = 'Factura'
        verbose_name_plural = 'Facturas'

    def save(self, *args, **kwargs):
        last = Invoice.objects.last()
        if not last:
            self.invoice_no = 1
        else:
            self.invoice_no = last.invoice_no + 1

        self.date = datetime.datetime.now()
        super(Invoice, self).save(*args, **kwargs)

    def __unicode__(self):
        return unicode(self.invoice_no)

class Banner(Titled):
    text = RichTextUploadingField(verbose_name='Texto', null=True, blank=True)
    image = models.ImageField(upload_to='banner', max_length=500, verbose_name='Imagen (Recomendado 1920x1080)')
    cropping = ImageRatioField('image', '1920x1080', verbose_name=u'Recorte visible')

    def get_img(self):
        return get_thumbnailer(self.image).get_thumbnail({'size': (1920, 1080), 'box': self.cropping, 'crop': True, 'detail': True }).url 

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = u'Banner'
        verbose_name_plural = 'Banners'