# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import *
from django.contrib import admin
from jet.admin import CompactInline
from django.contrib import messages
from image_cropping import ImageCroppingMixin



class CategoryAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ("id", "title",)
    list_editable = ("title", )

    fieldsets = (
            (u"Datos del Producto", {
                'fields': ('title', 'image', 'cropping', 'description' )
            }),
            (u"SEO", {
                'fields': ('meta_title', 'meta_description', 'meta_keywords',)
            }),
        )


class ProductImageInline(ImageCroppingMixin, CompactInline):
    model = ProductImage
    extra = 0


class ProductAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ("id", "title", "quantity", "price", 'old_price', 'featured', 'sales',)
    list_editable = ("title", "price", 'old_price', 'quantity', 'featured',)
    readonly_fields = ['sales', ]
    inlines = [ProductImageInline, ]

    fieldsets = (
            (u"Datos del Producto", {
                'fields': ('title', 'category', 'description', 'specification', ('quantity', 'price', 'old_price',), 'featured', 'sales', )
            }),
            (u"SEO", {
                'fields': ('meta_title', 'meta_description', 'meta_keywords',)
            }),
        )

    def sales(self, obj):
        return obj.sales.all().count()


class SaleAdmin(admin.ModelAdmin):
    list_filter = ("date", "product__title")
    list_display = ("id","date","get_product","price")
    readonly_fields = ['get_product', ]

    fields = ('date', 'price', 'product', )

    def get_product(self, obj):
        if obj.product:
            return obj.product.title
        return "Sin Producto"
    get_product.short_description = "Producto"


class InvoiceInline(CompactInline):
    model = Invoice
    extra = 0


class OrderAdmin(admin.ModelAdmin):
    readonly_fields = ('ref_token','date', 'list_products' )
    list_display = ('__unicode__', 'status', 'hour_collection', 'day_collection')
    inlines = [InvoiceInline,]
    list_filter = ('client__firstname', 'date')

    fieldsets = (
            (u"Datos del pedido", {
                'fields': (('date', 'client'), ('total', 'status'), 'notes', ('hour_collection', 'day_collection'), 'list_products')
            }),
            (u"Datos de facturación", {
                'fields': (('inv_firstname'), ('inv_phone'), ('inv_email'),)
            }),
        )

    #Gets all orders if is superuser, if not filter by client
    def get_queryset(self, request):
        clients = Client.objects.all()
        qs = super(OrderAdmin, self).get_queryset(request)
        #import ipdb; ipdb.set_trace()
        if request.user.is_superuser:
            return qs
        return qs.filter(client=request.user.profile)

    def list_products(self, obj):
        out = u"<table><thead><tr><th>Producto</th><th>Precio</th><th>Cantidad</th></tr></thead>"
        out += u"<tbody>"

        for prod in obj.products.split('/'):
            prod = prod.replace("(", "").replace(")", "").replace("L", "").split(',')
            product = Product.objects.get(id__exact=prod[0])
            out += ''.join("<tr><td>"+product.title+"</td><td>"+prod[1]+"</td><td>"+prod[2]+"</td></tr>")

        out += '</tbody></table>'
        return out

    list_products.short_description='Compra'
    list_products.allow_tags = True


class ClientAdmin(admin.ModelAdmin):
    list_display = ('user', 'firstname', 'address', 'phone',)
    list_editable = ('firstname', 'address', 'phone', )



class BannerAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ("id", "title",)
    list_editable = ("title",)

    fieldsets = (
            (u"Información del Banner", {
                'fields': ('title', 'text', 'image', 'cropping', )
            }),
        )


admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Sale, SaleAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Banner, BannerAdmin)