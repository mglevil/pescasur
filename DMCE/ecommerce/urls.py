from django.conf.urls import url
from ecommerce import views

urlpatterns = [
    url(r'^$', views.general, name="general"),
    url(r'^edit_cart/', views.edit_cart, name="edit_cart"),
    url(r'^carrito/', views.cart, name="my_cart"),
    url(r'^compra/', views.checkout, name="checkout"),
    url(r'^payment/', views.payment, name="payment"),
    url(r'^(?P<slug>[\w-]+)/$', views.dispatch_slug, name="dispatch_slug"),
]