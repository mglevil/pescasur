# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from ecommerce.models import *
from ecommerce.forms import *
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404, redirect, render_to_response
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from sermepa.models import SermepaIdTPV
from sermepa.forms import SermepaPaymentForm
from sermepa.signals import payment_was_successful
from django.core.mail import EmailMessage
from datetime import date
from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site
from django.conf import settings
from django.core.paginator import Paginator
from CMS.models import Config
from django.views.decorators.csrf import csrf_exempt
from ecommerce.tokens import account_activation_token
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm

"""ECOMMERCE VIEWS"""

def general(request):
    products = Product.objects.all()
    categories = Category.objects.all()

    return render(request, "ecommerce/general.html", locals())


def dispatch_slug(request, slug):
    #import ipdb; ipdb.set_trace()
    categories = Category.objects.all()
    #Products in cart
    if 'cart' in request.session and len(request.session['cart'].keys()) > 0:
        cart_items = Product.objects.filter(id__in = request.session['cart'].keys())
        cart_items = [(product, request.session['cart'][str(product.id)]) for product in cart_items]
        cart_tam = len(cart_items)


    slug = get_object_or_404(Slug, slug=slug)
    qs = Product.objects.filter(slug=slug)
    if qs.exists():
        product = qs[0]
        return render(request, "ecommerce/product.html", locals())   
    qs = Category.objects.filter(slug=slug)
    if qs.exists():
        content = qs[0] #contiene la categoria
        products_cat = Product.objects.filter(category=content.slug_id)
        cat = True
        return render(request, "ecommerce/general.html", locals())


def account(request):
    a_form = AuthenticationForm()
    su_form = SingUpForm()
    return render(request, "ecommerce/account.html", locals())


def cart(request):    
    categories = Category.objects.all()
    if 'cart' in request.session and len(request.session['cart'].keys()) > 0:
        cart_items = Product.objects.filter(id__in = request.session['cart'].keys())
        cart_subtotal = sum([product.price*request.session['cart'][str(product.id)] for product in cart_items])
        cart_items = [(product, request.session['cart'][str(product.id)]) for product in cart_items]
        cart_tam = len(cart_items)
        return render(request, "ecommerce/cart.html", locals())
    else:
        return HttpResponseRedirect('/')


@csrf_exempt
def edit_cart(request):
    categories = Category.objects.all()
    #data={}
    #if request.is_ajax():
    #si la cantidad viene en el post se asignara ese valor, sino se le asignara 1
    quantity = int(request.POST['quantity']) if 'quantity' in request.POST else 1
    #Añadir productos
    if 'add_product' in request.POST:
        if 'cart' in request.session:
            cart_dict = request.session['cart']
            cart_dict.update({request.POST['add_product']: quantity})
            request.session['cart'] = {}
            request.session['cart'] = cart_dict
        else:
            cart_dict = {}
            cart_dict[request.POST['add_product']] = quantity
            request.session['cart'] = {}
            request.session['cart'] = cart_dict
    #Eliminar producto
    elif 'del_product' in request.POST:
        del_product = request.POST['del_product']
        cart_dict = request.session['cart']
        cart_dict = {key: val for key, val in cart_dict.items() if key != del_product}    
        request.session['cart'] = {}
        request.session['cart'] = cart_dict  
    #Actualizar cantidad productos  
    elif 'upt_qty' in request.POST:
        upt_qty = int(request.POST['upt_qty'])
        cart_dict = request.session['cart']
        for key, val in cart_dict.items(): cart_dict.update({request.POST['update_product']: upt_qty})
        request.session['cart'] = {}
        request.session['cart'] = cart_dict   

    #Products in cart
    if 'cart' in request.session and len(request.session['cart'].keys()) > 0:
        cart_items = Product.objects.filter(id__in = request.session['cart'].keys())
        cart_items = [(product, request.session['cart'][str(product.id)]) for product in cart_items]
        cart_tam = len(cart_items)

    return render(request, "ecommerce/cart.html", locals())


def checkout(request):  
    if 'cart' in request.session and len(request.session['cart'].keys()) > 0:
        if request.user.is_anonymous:
            o_form = OrderForm()
        else:
            o_form = OrderForm(
                        initial={
                                'inv_firstname' : request.user.profile.firstname,
                                'inv_phone' : request.user.profile.phone,
                                'inv_email' : request.user.email,
                                'inv_card_id' : request.user.profile.card_id,
                                }
                            )

        cart_items = Product.objects.filter(id__in = request.session['cart'].keys())
        cart_subtotal = sum([product.price*request.session['cart'][str(product.id)] for product in cart_items])
        cart_items = [(product, request.session['cart'][str(product.id)]) for product in cart_items]
        
        return render(request, "ecommerce/checkout.html", locals())
    else:
        return HttpResponseRedirect('/')


@csrf_exempt
def payment(request):
    #data={}
    config = Config.objects.all()[0]
    #if request.is_ajax():
    cart_items = Product.objects.filter(id__in = request.session['cart'].keys())
    cart_subtotal = sum([product.price*request.session['cart'][str(product.id)] for product in cart_items])
    amount = cart_subtotal*100

    if request.POST['ship_firstname'] == "":
        order = Order(
                inv_firstname = request.POST['inv_firstname'],
                inv_phone = request.POST['inv_phone'],
                inv_email = request.POST['inv_email'],
                inv_card_id = request.POST['inv_card_id'],

                ship_firstname = request.POST['inv_firstname'],
                ship_phone = request.POST['inv_phone'],
                ship_email = request.POST['inv_email'],

                hour_collection = request.POST['hour_collection'],
                day_collection = request.POST['day_collection'],

                total = amount/100,
                client = request.user.profile if not request.user.is_anonymous else None,
                notes = request.POST['notes'],
                products ='/'.join(str(e) for e in [(product.id, product.price, request.session['cart'][str(product.id)]) for product in cart_items]),
                status = 'R',
                )
        order.save()
    else:
        order = Order(
                inv_firstname = request.POST['inv_firstname'],
                inv_phone = request.POST['inv_phone'],
                inv_email = request.POST['inv_email'],
                inv_card_id = request.POST['inv_card_id'],

                ship_firstname = request.POST['ship_firstname '],
                ship_phone = request.POST['ship_phone'],
                ship_email = request.POST['ship_email'],
                ship_card_id = request.POST['ship_card_id'],

                hour_collection = request.POST['hour_collection'],
                day_collection = request.POST['day_collection'],

                total = amount/100,
                client = request.user.profile if not request.user.is_anonymous else None,
                notes = request.POST['notes'],
                products ='/'.join(str(e) for e in [(product.id, product.price, request.session['cart'][str(product.id)]) for product in cart_items]),
                status = 'R',
                )
        order.save()
    
    #site = Site.objects.get_current()
    """
    sermepa_dict = {
        "Ds_Merchant_Titular": 'TITULAR',
        "Ds_Merchant_MerchantData": order.id, # id del Pedido o Carrito, para identificarlo en el mensaje de vuelta
        "Ds_Merchant_MerchantName": 'nombredelnegorio',
        "Ds_Merchant_ProductDescription": u'Compra de productos',
        "Ds_Merchant_Amount": int(amount),
        "Ds_Merchant_Terminal": settings.SERMEPA_TERMINAL,
        "Ds_Merchant_MerchantCode": settings.SERMEPA_MERCHANT_CODE,
        "Ds_Merchant_Currency": settings.SERMEPA_CURRENCY,
        "Ds_Merchant_MerchantURL": "https://%s%s" % (site.domain, reverse('sermepa_ipn')),
        "Ds_Merchant_UrlOK": "https://%s/tienda/pedido/%s?fr=True" % (site.domain, order.ref_token),
        "Ds_Merchant_UrlKO": "https://%s%s" % (site.domain, reverse('index')),
        "Ds_Merchant_Order": SermepaIdTPV.objects.new_idtpv()[-8:],
        "Ds_Merchant_TransactionType": '0',
      }

    form = SermepaPaymentForm(merchant_parameters=sermepa_dict)
    """
    #debug = True
    del request.session['cart']
    #return HttpResponse(render_to_response('ecommerce/form_sermepa.html', {'form': form, 'debug': debug}))
    #return HttpResponseRedirect('/')
    return render(request, "ecommerce/order_confirmed.html")


def order_confirmed(request, ref_token):
    order = get_object_or_404(Order, ref_token=ref_token)
    orders_products = []
    if order.status == "R":
        order.status = "C"
        order.save()

    for prod in order.products.split('/'):
        prod = prod.replace("(", "").replace(")", "").replace("L", "").split(',')
        product = get_object_or_404(product, id=prod[0])
        orders_products.append((product, prod[1], prod[2], order.date))

    if 'fr' in request.GET:
        fr = True 
 
    return render(request, "ecommerce/order_confirmed.html",locals())


def payment_ok(sender, **kwargs):
    code = sender.Ds_MerchantData
    config = Config.objects.all()[0]
    order = get_object_or_404(Order, id = int(code))
    order.status = "P"
    order.save()

    invoice = Invoice(order = order)
    invoice.total = order.total
    invoice.name = order.fac_firstname
    invoice.surnames = order.fac_lastname
    invoice.card_id = order.fac_card_id
    invoice.address = order.fac_address
    invoice.zip_code = order.fac_postal_code
    invoice.city = order.fac_city
    invoice.country = order.fac_country
    invoice.save()

    orders_products = []

    for prod in order.products.split('/'):
        prod = prod.replace("(", "").replace(")", "").replace("L", "").split(',')
        product = Product.objects.get(id=prod[0])
        orders_products.append((product, float(prod[1]), int(prod[2]), float(prod[1])*float(prod[2])))
        for i in range(int(prod[2])):
            sale = Sale()
            sale.product = product
            sale.price = product.price
            sale.save()
            product.quantity -= 1
            product.save()

    site = Site.objects.get_current()
    subject = '[Nombre del Negocio] - Confirmación de pedido'
    message = render_to_string('ecommerce/order_email.html', {
        'order': order,
        'domain': site.domain,
        'config': config,
        'orders_products': orders_products,
    })

    send_to = [order.fac_email,]
    email1 = EmailMessage(
            subject,
            message,
            settings.EMAIL_HOST_USER, 
            send_to,
        )
    email1.send()

    email2 = EmailMessage(
        subject,
        u'Se ha realizado un pedido en la tienda online, puede acceder al mismo en: "https://%s/admin/gestion/order/%s/change/"' % (site.domain, order.id),
        settings.EMAIL_HOST_USER, 
        [config.email,],
        )
    email2.send()

payment_was_successful.connect(payment_ok)


"""AUTHENTICATION VIEWS"""

def login_view(request):
    form_signup = SignUpForm()
    return render(request, "ecommerce/login.html", {'form_signup': form_signup})

def activate_view(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.profile.email_confirmed = True
        user.save()
        login(request, user)
        return redirect('index')
    else:
        return redirect('index')


def sing_up_view(request):
    su_form = SignUpForm(request.POST)
    if su_form.is_valid():
        user = su_form.save(commit=False)
        user.is_active = False
        user.save()
        current_site = get_current_site(request)
        subject = 'Activar cuenta en PescaSur Deportes'
        message = render_to_string('ecommerce/account_activation_email.html', {
            'user': user,
            'domain': current_site.domain,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': account_activation_token.make_token(user),
        })
        import ipdb; ipdb.set_trace()
        user.email_user(subject, message)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        

def auth_view(request):
    a_form = AuthenticationForm(data=request.POST)
    if a_form.is_valid():
        login(request, a_form.get_user())
    return redirect('account')


def logout_view(request):
    logout(request)
    for sesskey in request.session.keys():
        del request.session[sesskey]
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

