function addToCart(product_id, quantity){
    $.ajax({
        url:"/tienda/cart/",
          type:"POST",
          data:{
              'add_product': product_id,
              'quantity': quantity,
          },
          success: function(data){
            alert('Producto añadido correctamente');
          },
          error: function(){alert("Ha ocurrido un error inesperado...")}
    });
}

function delOfCart(product_id){
    $.ajax({
        url:"/tienda/cart/",
          type:"POST",
          data:{
              'del_product': product_id,
          },
          success: function(data){
            location.reload();
          },
          error: function(){alert("Ha ocurrido un error inesperado...")}
    });
}

