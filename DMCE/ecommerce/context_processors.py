# -*- coding: utf-8 -*-
from ecommerce.models import *

def basic_data(request):
    data = {'featured_prods': Product.objects.filter(featured = True)[:4],
            'last_prods': Product.objects.all()[:4],
            }   

    if 'cart' in request.session:
        cart_items = Product.objects.filter(id__in = request.session['cart'].keys())
        cart_subtotal = sum([product.price*request.session['cart'][str(product.id)] for product in cart_items])
        data['cart_items'] = [(product, request.session['cart'][str(product.id)]) for product in cart_items]
        data['cart_subtotal'] = cart_subtotal

    return data