# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Client, Order, HOUR_CHOICES, DAY_CHOICES
from django import forms
from django.utils.encoding import force_text


class SignUpForm(UserCreationForm):
    
    email = forms.EmailField(max_length=254)

    def clean_email(self):
        data = self.cleaned_data['email']
        if User.objects.filter(email=data).exists():
            raise forms.ValidationError("Ya existe un usuario con este email.")
        return data

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2',)

class ClientForm(forms.ModelForm):
    firstname = forms.CharField(label="Nombre")
    lastname = forms.CharField(label="Apellidos")
    phone = forms.CharField(label="Teléfono")
    country = forms.CharField(label="País")
    city = forms.CharField(label="Ciudad")
    postal_code = forms.CharField(label="Código Postal")
    address = forms.CharField(label="Dirección")
    card_id = forms.CharField(label=u'DNI/NIF/CIF')


    class Meta:
        model = Client
        fields = ('firstname', 'lastname', 'phone', 'country', 'city', 'postal_code', 'address', 'card_id')



class OrderForm(forms.Form):
    inv_firstname = forms.CharField(label="Nombre", required=True)
    #inv_lastname = forms.CharField(label="Apellidos", required=True)
    inv_phone = forms.CharField(label=u"Teléfono", required=True)
    #inv_country = forms.CharField(label=u"País", required=True)
    #inv_city = forms.CharField(label="Ciudad", required=True)
    #inv_postal_code = forms.CharField(label="Código Postal", required=True)
    #inv_address = forms.CharField(label=u"Dirección", required=True)
    inv_email = forms.EmailField(label=u'Correo electrónico', required=True)
    inv_card_id = forms.CharField(label=u'DNI/NIF/CIF', required=True)

    hour_collection = forms.ChoiceField(label=u'Hora de recogida', choices=HOUR_CHOICES, required=True)
    day_collection = forms.ChoiceField(label=u'Día de recogida', choices=DAY_CHOICES, required=True)
    notes = forms.CharField(label="Notas sobre el pedido", widget=forms.Textarea(attrs={'cols' : "49", 'rows': "5", }), required=False)
    #dif_ship = forms.BooleanField(label=u"Los datos de envío no coinciden con los de facturación", required=False)
    
    ship_firstname = forms.CharField(label="Nombre", widget=forms.HiddenInput(), required=False)
    #ship_lastname = forms.CharField(label="Apellidos", required=False)
    ship_phone = forms.CharField(label="Teléfono", required=False)
    #ship_country = forms.CharField(label="País", required=False)
    #ship_city = forms.CharField(label="Ciudad", required=False)
    #ship_postal_code = forms.CharField(label="Código Postal", required=False)
    #ship_address = forms.CharField(label="Dirección", required=False)
    ship_email = forms.EmailField(label=u'Correo electrónico', required=False)

    #añadir placeholders
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['inv_firstname'].widget.attrs.update({'placeholder': 'Nombre'})
        self.fields['inv_phone'].widget.attrs.update({'placeholder': 'Teléfono'})
        self.fields['inv_email'].widget.attrs.update({'placeholder': 'Email'})
        self.fields['inv_card_id'].widget.attrs.update({'placeholder': 'DNI/CIF/NIF'})
        self.fields['notes'].widget.attrs.update({'placeholder': 'Notas sobre la reserva'})
    
